import torch
import torch.nn as nn
import torchvision as tv

import models
import manager

if __name__ == '__main__':

    g = models.Generator()
    g_loss = nn.BCELoss()
    g_opt = torch.optim.Adam(g.parameters(), lr=2e-4)

    d = models.Discriminator()
    d_loss = nn.BCELoss()
    d_opt = torch.optim.Adam(d.parameters(), lr=2e-4)

    data_set = tv.datasets.MNIST(
        '~/.ntdata',
        train=True,
        download=True,
        transform=tv.transforms.Compose([
            tv.transforms.ToTensor(),
           # tv.transforms.Normalize((0.13066,), (0.30810,))
           tv.transforms.Normalize(mean=(0.5,), std=(0.5,))
        ])
    )
    data_loader = torch.utils.data.DataLoader(data_set, batch_size=125, shuffle=False)

    device = torch.device("cuda")

    manager = manager.Manager(
        g,d,
        g_loss, d_loss,
        g_opt, d_opt,
        data_loader,
        device
    )


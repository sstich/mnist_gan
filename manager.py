import logging
import numpy as np
from matplotlib import pyplot as plt
import time
import torch
from torch.autograd import Variable

class Manager(object):

    def __init__(self, generator, discriminator,
                       gen_loss, disc_loss,
                       gen_opt, disc_opt,
                       data_loader,
                       device):
        self.gen = generator
        self.disc = discriminator
        self.gen_loss = gen_loss
        self.disc_loss = disc_loss
        self.gen_opt = gen_opt
        self.disc_opt = disc_opt
        self.data_loader = data_loader
        self.device = device

        self.gen.to(self.device)
        self.disc.to(self.device)

    def train_discriminator(self, image_batch):

        self.disc.zero_grad()

        # Train on real images.
        real_images = Variable(image_batch).to(self.device)
        ones = Variable(torch.ones(self.data_loader.batch_size)).to(self.device).view(-1)
        disc_predictions_on_real = self.disc(real_images).view(-1)
        disc_real_loss = self.disc_loss(
            disc_predictions_on_real,
            ones
        )
        disc_real_loss.backward()

        # Train on fake images.
        noise = Variable(
            torch.rand(self.data_loader.batch_size, self.gen.IN_DIMENSION)
        ).to(self.device)
        with torch.no_grad():
            fake_images_raw = self.gen(noise)
        fake_images = Variable(fake_images_raw).to(self.device)
        zeroes = Variable(torch.zeros(self.data_loader.batch_size)).to(self.device).view(-1)
        disc_predictions_on_fake = self.disc(fake_images).view(-1)
        disc_fake_loss = self.disc_loss(
            disc_predictions_on_fake,
            zeroes
        )

        # Compute loss & backprop
        total_loss = disc_real_loss + disc_fake_loss
        disc_fake_loss.backward()
        self.disc_opt.step()

        return (
            total_loss.detach().cpu().numpy(),
            disc_predictions_on_real.detach().cpu().numpy(),
            disc_predictions_on_fake.detach().cpu().numpy()
        )

    def train_generator(self):

        self.gen.zero_grad()

        noise = Variable(
            torch.rand(self.data_loader.batch_size, self.gen.IN_DIMENSION)
        ).to(self.device)
        fake_images = self.gen(noise)
       # disc_predictions_on_fake_raw = self.disc(fake_images)
       # with torch.no_grad():
       #     disc_predictions_on_fake_raw = self.disc(fake_images.detach())
       # disc_predictions_on_fake = Variable(disc_predictions_on_fake_raw).to(self.device)

        disc_predictions_on_fake = self.disc(fake_images).view(-1)

        ones = Variable(torch.ones(self.data_loader.batch_size)).to(self.device).view(-1)
        loss = self.gen_loss(
            disc_predictions_on_fake,
            ones
        )
        loss.backward()
        self.gen_opt.step()
        return loss.detach().cpu().numpy()

    def train_epoch(self):

        start_time = time.time()

        # Keep track of the losses for this epoch
        epoch_disc_loss = 0
        epoch_gen_loss = 0
        epoch_disc_on_real = 0
        epoch_disc_on_fake = 0
        for i, (images, labels) in enumerate(self.data_loader):

            disc_loss, disc_on_real, disc_on_fake = self.train_discriminator(images)

            epoch_disc_on_real += disc_on_real.sum()
            epoch_disc_on_fake += disc_on_fake.sum()
            epoch_disc_loss += disc_loss
            epoch_gen_loss += self.train_generator()

        end_time = time.time()

        return {
            'epoch_gen_loss': epoch_gen_loss,
            'epoch_disc_loss': epoch_disc_loss,
            'epoch_disc_on_real': epoch_disc_on_real/60000.0,
            'epoch_disc_on_fake': epoch_disc_on_fake/60000.0,
            'epoch_time': end_time - start_time
        }

    def train_all_epochs(self, num_epochs):

        constant_gen_input = Variable(torch.randn(16, self.gen.IN_DIMENSION)).to(self.device)

        epoch_count = 0
        epoch_stat_rows = []
        epoch_gen_outputs = []
        while epoch_count < num_epochs:
            epoch_results = self.train_epoch()
            epoch_stat_rows.append(epoch_results)
            print(f"Epoch {epoch_count} Gen  Loss: {epoch_results['epoch_gen_loss']:.2f}")
            print(f"Epoch {epoch_count} Disc Loss: {epoch_results['epoch_disc_loss']:.2f}")
            print(f"Epoch {epoch_count} D(x)     : {epoch_results['epoch_disc_on_real']:.2f}")
            print(f"Epoch {epoch_count} D(G(z))  : {epoch_results['epoch_disc_on_fake']:.2f}")

            this_epoch_gen_output = self.gen(constant_gen_input)
            epoch_gen_outputs.append(this_epoch_gen_output.detach().cpu().view(16, 28, 28))
            image = epoch_gen_outputs[-1]
            x = np.block([
                [image[0],  image[1],  image[2],  image[3]],
                [image[4],  image[5],  image[6],  image[7]],
                [image[8],  image[9],  image[10], image[11]],
                [image[12], image[13], image[14], image[15]],
            ])

            plt.imshow(x)
            plt.show()
            print("\n")
            epoch_count += 1

        return epoch_stat_rows, epoch_gen_outputs

import torch.nn as nn


class Discriminator(nn.Module):

    IN_DIMENSION = 784
    OUT_DIMENSION = 1

    def __init__(self):
        super().__init__()
        self.model = nn.Sequential(
            nn.Linear(Discriminator.IN_DIMENSION, 1024),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Dropout(0.3),
            nn.Linear(1024, 512),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Dropout(0.3),
            nn.Linear(512, 256),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Dropout(0.3),
            nn.Linear(256, Discriminator.OUT_DIMENSION),
            nn.Sigmoid()
        )

    def forward(self, x):
        out = self.model(x.view(x.size(0), 784))
        out = out.view(out.size(0), -1)
        return out

class Generator(nn.Module):

    IN_DIMENSION = 100
    OUT_DIMENSION = 784

    def __init__(self):
        super().__init__()
        self.model = nn.Sequential(
            nn.Linear(Generator.IN_DIMENSION, 256),
            nn.LeakyReLU(0.2, inplace=True),
            nn.BatchNorm1d(256),
            nn.Linear(256, 512),
            nn.LeakyReLU(0.2, inplace=True),
            nn.BatchNorm1d(512),
            nn.Linear(512, 1024),
            nn.LeakyReLU(0.2, inplace=True),
            nn.BatchNorm1d(1024),
            nn.Linear(1024, Generator.OUT_DIMENSION),
            nn.Tanh()
        )

    def forward(self, x):
        x = x.view(x.size(0), Generator.IN_DIMENSION)
        out = self.model(x)
        return out


import logging
import time
import torch
from torch.autograd import Variable

class Manager(object):

    def __init__(self, generator, discriminator,
                       gen_loss, disc_loss,
                       gen_opt, disc_opt,
                       data_loader,
                       device):
        self.gen = generator
        self.disc = discriminator
        self.gen_loss = gen_loss
        self.disc_loss = disc_loss
        self.gen_opt = gen_opt
        self.disc_opt = disc_opt
        self.data_loader = data_loader
        self.device = device

        self.gen.to(self.device)
        self.disc.to(self.device)


    def train_epoch(self, train_disc=True):

        start_time = time.time()

        one = torch.FloatTensor([1]).to(self.device)
        mone = one * -1

        # Keep track of the losses for this epoch
        epoch_disc_loss = 0
        epoch_gen_loss = 0
        for i, (images, labels) in enumerate(self.data_loader):

            real_images = Variable(images).to(self.device)

            ###########################
            # Train the discriminator #
            ###########################

            for i in range(1):

                self.disc.zero_grad()
                for p in self.disc.parameters():
                    p.data.clamp_(-10, 10)
                    p.requires_grad = True

                noise_for_disc = Variable(# Normal noise of size [batch_size] x [generator input dimension]
                        torch.randn(self.data_loader.batch_size, self.gen.IN_DIMENSION, device=self.device)
                )*10
                with torch.no_grad():
                    fk = self.gen(noise_for_disc)
                fake_images = Variable(fk.data)

                disc_on_real = self.disc(real_images)
                disc_on_fake = self.disc(fake_images)

                disc_on_real.mean(0).backward(one)
                disc_on_fake.mean(0).backward(mone)

                # A perfect disc should map all real images to 1
                # real_loss = self.disc_loss(
                #        disc_on_real.view(disc_on_real.size(0)),
                #        Variable(torch.ones(self.data_loader.batch_size, device=self.device))
                #        )
                #real_loss.backward(one)

                # A perfect disc should map all fake images to 0
                # fake_loss = self.disc_loss(
                #        disc_on_fake.view(disc_on_fake.size(0)),
                #        Variable(torch.zeros(self.data_loader.batch_size, device=self.device))
                #        )
                # fake_loss.backward(mone)

                real_loss = disc_on_real.mean(0)
                fake_loss = disc_on_fake.mean(0)
                total_disc_loss = real_loss - fake_loss
                # total_disc_loss.backward()
                self.disc_opt.step()
                epoch_disc_loss += total_disc_loss

            #######################
            # Train the generator #
            #######################
            self.gen.zero_grad()

            for p in self.disc.parameters():
                p.requires_grad = False

            noise_for_gen = Variable(torch.randn(self.data_loader.batch_size,
                                                 self.gen.IN_DIMENSION,
                                                 device=self.device))*10
            fake_images = self.gen(noise_for_gen)
            disc_scores = self.disc(fake_images)

            # gen_loss = self.gen_loss(disc_scores.view(disc_scores.size(0)),
            #         Variable(torch.ones(self.data_loader.batch_size, device=self.device))
            #        )
            # gen_loss.backward(one)
            disc_scores.mean(0).backward(one)
            gen_loss = disc_scores.mean(0)
            self.gen_opt.step()

            epoch_gen_loss += gen_loss

        end_time = time.time()

        return {
            'epoch_gen_loss': epoch_gen_loss.detach().cpu().numpy(),
            'epoch_disc_loss': epoch_disc_loss.detach().cpu().numpy(),
            'epoch_time': end_time - start_time
        }

    def train_all_epochs(self, num_epochs):

        constant_gen_input = Variable(torch.randn(16, self.gen.IN_DIMENSION)).to(self.device)

        epoch_count = 0
        epoch_stat_rows = []
        epoch_gen_outputs = []
        while epoch_count < num_epochs:
            train_disc = True
            epoch_results = self.train_epoch(train_disc)
            epoch_stat_rows.append(epoch_results)
            print(f"Epoch {epoch_count} Gen  Loss: {epoch_results['epoch_gen_loss'][0]:.2f}")
            print(f"Epoch {epoch_count} Disc Loss: {epoch_results['epoch_disc_loss'][0]:.2f}")
            print("\n")

            with torch.no_grad():
                this_epoch_gen_output = self.gen(constant_gen_input)
                epoch_gen_outputs.append(this_epoch_gen_output.detach().cpu().view(16, 28, 28))

            epoch_count += 1

        return epoch_stat_rows, epoch_gen_outputs

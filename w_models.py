import torch.nn as nn

class Discriminator(nn.Module):

    IN_DIMENSION = 784
    OUT_DIMENSION = 1

    def __init__(self):
        super().__init__()
        self.model = nn.Sequential(
            nn.Linear(Discriminator.IN_DIMENSION, 1024),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Dropout(0.3),
            nn.Linear(1024, 512),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Dropout(0.3),
            nn.Linear(512, 256),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Dropout(0.3),
            nn.Linear(256, Discriminator.OUT_DIMENSION),
        )

    def forward(self, x):
        out = self.model(x.view(x.size(0), 784))
        out = out.view(out.size(0), -1)
        return out

class Generator(nn.Module):

    IN_DIMENSION = 100
    OUT_DIMENSION = 784

    def __init__(self):
        super().__init__()
        self.model = nn.Sequential(
            nn.Linear(Generator.IN_DIMENSION, 512),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Linear(512, 512),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Linear(512, 512),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Linear(512, Generator.OUT_DIMENSION),
        )

    def forward(self, x):
        x = x.view(x.size(0), Generator.IN_DIMENSION)
        out = self.model(x)
        return out

if __name__ == '__main__':
    g = Generator()
    d = Discriminator()
